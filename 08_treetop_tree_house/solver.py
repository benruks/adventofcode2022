class ForestMap():
    def __init__(self, path):
        print('input:', path)
        with open(path) as f:
            self._tree_heights = [list(map(int, list(e))) for e in f.read().split('\n')]
            self._x_len = len(self._tree_heights[0])
            self._y_len = len(self._tree_heights)

    def _get_hight_of_tree_at_pos(self, x, y):
        '''origin bottom left (x-right, y-up)'''
        if x >= 0 and x < self._x_len and y >= 0 and y < self._y_len:
            return self._tree_heights[self._y_len-y-1][x]
        else:
            return -1

    def _is_visible(self, x, y):
        height = self._get_hight_of_tree_at_pos(x, y)
        
        right_block = False
        for x_coord in range(x+1, self._x_len, 1):
            if self._get_hight_of_tree_at_pos(x_coord, y) >= height:
                right_block = True
                break

        left_block = False
        for x_coord in range(x-1, -1, -1):
            if self._get_hight_of_tree_at_pos(x_coord, y) >= height:
                left_block = True
                break

        top_block = False
        for y_coord in range(y+1, self._y_len, 1):
            if self._get_hight_of_tree_at_pos(x, y_coord) >= height:
                top_block = True
                break

        bottom_block = False
        for y_coord in range(y-1, -1, -1):
            if self._get_hight_of_tree_at_pos(x, y_coord) >= height:
                bottom_block = True
                break

        return not all([right_block, left_block, top_block, bottom_block])


    def get_amount_of_visible_trees(self):
        amount = 0
        for x in range(self._x_len):
            for y in range(self._y_len):
                amount += 1 if self._is_visible(x, y) else 0
        return amount

    def _get_scenic_score(self, x, y):
        height = self._get_hight_of_tree_at_pos(x, y)
        
        right_score = 0
        for x_coord in range(x+1, self._x_len, 1):
            right_score += 1
            if self._get_hight_of_tree_at_pos(x_coord, y) >= height:
                break

        left_score = 0
        for x_coord in range(x-1, -1, -1):
            left_score += 1
            if self._get_hight_of_tree_at_pos(x_coord, y) >= height:
                break

        top_score = 0
        for y_coord in range(y+1, self._y_len, 1):
            top_score += 1
            if self._get_hight_of_tree_at_pos(x, y_coord) >= height:
                break

        bottom_score = 0
        for y_coord in range(y-1, -1, -1):
            bottom_score += 1
            if self._get_hight_of_tree_at_pos(x, y_coord) >= height:
                break

        return right_score * left_score * top_score * bottom_score

    def get_highest_scenic_score(self):
        max_score = 0
        for x in range(self._x_len):
            for y in range(self._y_len):
                score = self._get_scenic_score(x, y)
                max_score = score if score > max_score else max_score
        return max_score


if __name__ == "__main__":
    fm = ForestMap('data')

    # puzzle 1
    print(fm.get_amount_of_visible_trees())

    # puzzle 2
    print(fm.get_highest_scenic_score())
