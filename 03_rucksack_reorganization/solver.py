class RucksackReorganizer():
    def __init__(self, path):
        print('input:', path)
        with open(path) as f:
            self.rucksacks = f.read().split('\n')

    def get_priorities_sum_1(self):
        psum = 0
        for rucksack in self.rucksacks:
            c1 = rucksack[:int(len(rucksack)/2)]
            c2 = rucksack[int(len(rucksack)/2):]
            for c1_item in c1:
                if c1_item in c2:
                    psum += self._get_priority_of_item(c1_item)
                    break
        return psum

    def get_priorities_sum_2(self):
        psum = 0
        cnt = 0
        group = []
        for rucksack in self.rucksacks:
            group.append(rucksack)
            cnt += 1
            if cnt == 3:
                badge = self._get_group_badge(group)
                psum += self._get_priority_of_item(badge)
                cnt = 0
                group = []
        return psum

    def _get_group_badge(self, group):
        if len(group) != 3:
            print("wrong group size!")
            exit
        for item in group[0]:
            if item in group[1] and item in group[2]:
                return item
    
    def _get_priority_of_item(self, item_char):
        if len(item_char) != 0 and type(item_char) != str:
            print('wrong input!')
            exit
        if item_char.islower():
            return ord(item_char) - 96
        elif item_char.isupper():
            return ord(item_char) - 38

if __name__ == "__main__":
    rr = RucksackReorganizer('data')
    
    # puzzle 1
    print(rr.get_priorities_sum_1())

    # puzzle 2
    print(rr.get_priorities_sum_2())