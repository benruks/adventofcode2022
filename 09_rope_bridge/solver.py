class RopeBridge():
    def __init__(self, path, num_tails=1):
        self.num_tails = num_tails
        print('input:', path, 'tails:', num_tails)
        with open(path) as f:
            self.motion_series = f.readlines()
            self._pos = [[(0, 0)]] # first entry is the head
            self._pos.extend([[(0, 0)] for n in range(num_tails)])
            self._process_head_positions()
            self._process_tail_positions()

    def _process_head_positions(self):
        for move in self.motion_series:
            direction = move.split(' ')[0]
            amount = int(move.split(' ')[1])
            if direction == 'R':
                self._pos[0].extend([(self._pos[0][-1][0] + a, self._pos[0][-1][1]) for a in range(1, amount+1, 1)])
            if move.split(' ')[0] == 'L':
                self._pos[0].extend([(self._pos[0][-1][0] - a, self._pos[0][-1][1]) for a in range(1, amount+1, 1)])
            if move.split(' ')[0] == 'U':
                self._pos[0].extend([(self._pos[0][-1][0], self._pos[0][-1][1] + a) for a in range(1, amount+1, 1)])
            if move.split(' ')[0] == 'D':
                self._pos[0].extend([(self._pos[0][-1][0], self._pos[0][-1][1] - a) for a in range(1, amount+1, 1)])


    def _process_tail_positions(self):
        for i in range(self.num_tails):
            for head in self._pos[i]:
                if self._get_distance_sqrd(head, self._pos[i+1][-1]) > 2:
                    possible_ts = []
                    for x in [-1, 0, 1]:
                        for y in [-1, 0, 1]:
                            possible_ts.append((self._pos[i+1][-1][0] + x, self._pos[i+1][-1][1] + y))
                    best_next_t = min([(possible_t, self._get_distance_sqrd(head, possible_t)) for possible_t in possible_ts], key=lambda p: p[1])
                    self._pos[i+1].append(best_next_t[0])
                    if best_next_t[1] > 2:
                        print('this should not be possible')
                        exit()

    def _get_distance_sqrd(self, p1, p2):
        return (p1[0] - p2[0])**2 + (p1[1] - p2[1])**2


    def get_amount_of_visited_pos_of_tail(self):
        return len(set(self._pos[-1]))

if __name__ == "__main__":
    # data = 'example_data'
    # data = 'example_data_2'
    data = 'data'
    
    # puzzle 1
    rb1 = RopeBridge(data)
    print('There are', rb1.get_amount_of_visited_pos_of_tail(), 'positions the tail visited at least once')

    # puzzle 2
    rb2 = RopeBridge(data, num_tails=9)
    print('There are', rb2.get_amount_of_visited_pos_of_tail(), 'positions the tail visited at least once')