from enum import Enum

class PacketMarker(Enum):
    START = 1
    MESSAGE = 2


class SignalDecoder():
    def __init__(self, path):
        print('input:', path)
        with open(path) as f:
            self.datastream_buffer = f.read()

    def get_start_of_marker(self, marker_type : PacketMarker) -> int:
        unique_len = 0
        if marker_type == PacketMarker.START:
            unique_len = 4
        if marker_type == PacketMarker.MESSAGE:
            unique_len = 14

        for i in range(len(self.datastream_buffer)):
            if i+unique_len > len(self.datastream_buffer):
                print("no marker found")
                return -1

            if self._check_unique(self.datastream_buffer[i:i+unique_len]):
                return i+unique_len

        print("no marker found")
        return -1


    def _check_unique(self, str):
        for i in range(len(str)):
            for j in range(i + 1,len(str)):
                if(str[i] == str[j]):
                    return False
        return True

if __name__ == "__main__":
    sd = SignalDecoder('data')

    # puzzle 1
    print(sd.get_start_of_marker(PacketMarker.START))

    # puzzle 2
    print(sd.get_start_of_marker(PacketMarker.MESSAGE))