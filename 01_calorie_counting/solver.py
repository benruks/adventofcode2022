class CalorieCounter():
    def __init__(self, path):
        print('input:', path)
        with open(path) as f:
            self.elves_list = [c.split('\n') for c in f.read().split('\n\n')]

    def get_sum_of_highest_calorie_count(self):
        if self.elves_list is not None:
            calorie_sum = [sum(list(map(int, s))) for s in self.elves_list]
            return max(calorie_sum)
        else:
            return None

    def get_sum_of_top_three_calories_count(self):
        if self.elves_list is not None:
            sorted_calorie_sum = sorted([sum(list(map(int, s))) for s in self.elves_list], reverse=True)
            return sum(sorted_calorie_sum[:3]) 


if __name__ == "__main__":
    cc = CalorieCounter('data')

    # part one
    print('Highest calorie count:', cc.get_sum_of_highest_calorie_count())

    # part two
    print('Top three calorie count:', cc.get_sum_of_top_three_calories_count())