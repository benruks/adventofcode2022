class CampCleaner():
    def __init__(self, path):
        print('input:', path)
        with open(path) as f:
            section_assignments_pairs = [c.split(',') for c in f.read().split('\n')]
            self.section_assignments_pairs_full = []
            for section_assignments_pair in section_assignments_pairs:
                elve1 = [i for i in range(int(section_assignments_pair[0].split('-')[0]), int(section_assignments_pair[0].split('-')[1])+1)]
                elve2 = [i for i in range(int(section_assignments_pair[1].split('-')[0]), int(section_assignments_pair[1].split('-')[1])+1)]
                self.section_assignments_pairs_full.append([elve1, elve2])

    def get_num_of_fully_contain_pair(self):
        num = 0
        for pair in self.section_assignments_pairs_full:
            if set(pair[0]).issubset(pair[1]) or set(pair[1]).issubset(pair[0]):
                num += 1
        return num

    def get_num_of_overlapping_pairs(self):
        num = 0
        for pair in self.section_assignments_pairs_full:
            for section in pair[0]:
                if section in pair[1]:
                    num += 1
                    break
        return num

if __name__ == "__main__":
    cc = CampCleaner('data')
    
    # puzzle 1
    print(cc.get_num_of_fully_contain_pair())

    # puzzle 2
    print(cc.get_num_of_overlapping_pairs())