OPPONENT_PLAY = {'A': 'Rock', 'B': 'Paper', 'C': 'Scissor'}
NEEDED_OUTCOME = {'X': 'loose', 'Y': 'draw', 'Z': 'win'}

SHAPE_SCORE = {'Rock': 1, 'Paper': 2, 'Scissor': 3}
OUTCOME_SCORE = {'lost': 0, 'draw': 3, 'win': 6}

class RockPaperScissor():
    def __init__(self, path):
        print('input:', path)
        with open(path) as f:
            self.strategy_guide = [c.split(' ') for c in f.read().split('\n')]

    def _get_score_of_one_round(self, play):
        oponent_pick = OPPONENT_PLAY[play[0]]
        my_pick = self._get_shape_to_play(oponent_pick, NEEDED_OUTCOME[play[1]])

        if oponent_pick == my_pick: # draw
            score = OUTCOME_SCORE['draw'] + SHAPE_SCORE[my_pick]        
        elif self._beats(oponent_pick, my_pick): # lost
            score = OUTCOME_SCORE['lost'] + SHAPE_SCORE[my_pick]
        else: # win
            score = OUTCOME_SCORE['win'] + SHAPE_SCORE[my_pick]
        
        print(oponent_pick, '-', my_pick, '->', score)
        return score

    def _get_shape_to_play(self, opponent_pick, needed_outcome):
        if opponent_pick == 'Rock':
            if needed_outcome == 'loose':
                return 'Scissor'
            elif needed_outcome == 'draw':
                return 'Rock'
            elif needed_outcome == 'win':
                return 'Paper'
        if opponent_pick == 'Paper':
            if needed_outcome == 'loose':
                return 'Rock'
            elif needed_outcome == 'draw':
                return 'Paper'
            elif needed_outcome == 'win':
                return 'Scissor'
        if opponent_pick == 'Scissor':
            if needed_outcome == 'loose':
                return 'Paper'
            elif needed_outcome == 'draw':
                return 'Scissor'
            elif needed_outcome == 'win':
                return 'Rock'

    def _beats(self, shape1, shape2):
        if shape1 == 'Rock':
            return True if shape2 == 'Scissor' else False
        if shape1 == 'Paper':
            return True if shape2 == 'Rock' else False
        if shape1 == 'Scissor':
            return True if shape2 == 'Paper' else False
        

    def get_total_score(self):
        score = 0
        for rnd in self.strategy_guide:
            score += self._get_score_of_one_round(rnd)
        return score
    


if __name__ == "__main__":
    rpc = RockPaperScissor('data')
    print(rpc.get_total_score())