class Move():
    def __init__(self, amount, source, destination):
        self.amount = amount
        self.source = source
        self.destination = destination

    @classmethod
    def from_raw_string(cls, input):
        return cls(int(input.split(' ')[1]), int(input.split(' ')[3]), int(input.split(' ')[5]))

    def __repr__(self) -> str:
        return "move {amount} from {source} to {destination}".format(amount=self.amount, source=self.source, destination=self.destination)

class SupplyStackArranger():
    def __init__(self, path, crane_model):
        self.crane_model = crane_model
        print('input:', path)
        with open(path) as f:
            input = f.read().split('\n')
            stack_raw = input[:input.index('')]
            procedures_raw = input[input.index('')+1:]
            self.procedures = [Move.from_raw_string(move_raw) for move_raw in procedures_raw]
            self.supplystack = self._get_supply_stack_from_input(stack_raw)

    def _get_supply_stack_from_input(self, stack_raw):
        self.stack_amount = len(stack_raw[-1].replace(' ', ''))
        stack = []
        for s in range(self.stack_amount):
            stack.append([])
            for line in stack_raw:
                item = line[s+s*3:s+s*3+3].replace(' ', '')
                if len(item) != 0:
                    if item.isnumeric():
                        stack[s].insert(0, int(item))
                    else:
                        stack[s].insert(0, item)
        return stack

    def _move_one_item(self, source, destination):
        self.supplystack[destination-1].append(self.supplystack[source-1][-1])
        del self.supplystack[source-1][-1]

    def _move_all_items_at_once(self, x, source, destination):
        for t in reversed(range(x)):
            self.supplystack[destination-1].append(self.supplystack[source-1][-(t+1)])
            del self.supplystack[source-1][-(t+1)]

    def _move_x_items(self, x, source, destination):
        if self.crane_model == '9000': # move one crate at a time
            for i in range(x):
                self._move_one_item(source, destination)
        if self.crane_model == '9001': # move all crates at once
            self._move_all_items_at_once(x, source, destination)

    def execute_procedure_to_stack(self):
        for move in self.procedures:
            self._move_x_items(move.amount, move.source, move.destination)

    def get_top_crates(self):
        top = ''
        for stack in self.supplystack:
            top += stack[-1][1]
        return top
if __name__ == "__main__":
    # ssa = SupplyStackArranger('data', crane_model='9000') # puzzle 1
    ssa = SupplyStackArranger('data', crane_model='9001') # puzzle 2

    print('\n--- stack ---')
    for stack in ssa.supplystack:
        print(stack)

    print('\n--- rearranged stack after {} moves ---'.format(len(ssa.procedures)))
    ssa.execute_procedure_to_stack()
    for stack in ssa.supplystack:
        print(stack)

    print('\n--- solution ---')
    print('top crates:', ssa.get_top_crates())