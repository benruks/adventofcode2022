class FileSystem():
    def __init__(self, path):
        print('input:', path)
        with open(path) as f:
            terminal_output = f.read().split('\n')
            self.file_system = self._reconstruct_file_system(terminal_output)

    def _reconstruct_file_system(self, terminal_output):
        fs = {}
        wd = ''
        for output in terminal_output:
            #print('output:', output)
            out = output.split(' ')
            if out[0] == '$':
                if out[1] == 'cd':
                    path = out[2]
                    if path == '..':
                        wd = '/'.join(wd.split('/')[:-1])
                        if len(wd) == 0:
                            wd = '/'
                    else:
                        if path.startswith('/'):
                            wd = path
                        else:
                            if wd.endswith('/'):
                                wd += path
                            else:
                                wd += '/' + path
                        if wd not in fs.keys():
                            fs[wd] = []
                    
                if out[1] == 'ls':
                    pass

            elif out[0] == 'dir':
                if not wd.endswith('/'):
                    if (wd+'/'+out[1]) not in fs.keys():
                        fs[wd+'/'+out[1]] = []
                else:
                    if (wd+out[1]) not in fs.keys():
                        fs[wd+out[1]] = []

            elif out[0].isnumeric():
                fs[wd].append((out[1], int(out[0])))

            #print('pwd:', wd)
            #print(fs)
            #print('------------------')
            #if any(['//' in key for key in fs.keys()]):
            #    print("STOOOOP")
            #    exit()
            #input()

        return fs

    def _get_full_size_of_folder(self, folder):
        size = 0
        for directory in self.file_system.keys():
            if directory.startswith(folder):
                size += sum([element[1] for element in self.file_system[directory]])
        #print(folder, '->', size)
        return size

    def calculate_total_sum_of_directories(self, threshold=100000):
        total = 0
        for directory in self.file_system.keys():
            size = self._get_full_size_of_folder(directory)
            total += size if size < threshold else 0
        return total


    def find_folder_to_delete(self, required_space=30000000, available_space=70000000):
        used_space = self._get_full_size_of_folder('/')
        free_space = available_space - used_space
        to_be_freed = required_space - free_space
        
        min_folder = min(filter(lambda el: el[1] > to_be_freed, [(path, self._get_full_size_of_folder(path)) for path in self.file_system.keys()]), key = lambda t: t[1])
        print('Delete folder:', min_folder[0], 'to free up', min_folder[1], 'space (then available:', free_space + min_folder[1], ')')
    

if __name__ == "__main__":
    fs = FileSystem('data')

    # puzzle 1
    print(fs.calculate_total_sum_of_directories())
    
    #puzzle 2
    fs.find_folder_to_delete()